import angular from 'angular';
import routing from './main.routes';
import uiRouter from 'angular-ui-router';

export class MainController {

  /*@ngInject*/
  constructor(scrollTo) {
    this.scrollTo = scrollTo;
  }

  $onInit() {
    this.scrollTo('top');
  }
}

export default angular.module('acosoEnLineaApp.main', [uiRouter])
  .config(routing)
  .component('main', {
    template: require('./main.html'),
    controller: MainController
  })
  .name;
