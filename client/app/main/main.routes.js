'use strict';

export default function routes($stateProvider) {
  'ngInject';
  $stateProvider.state('main', {
    url: '/',
    template: '<main></main>',
    abstract: true
  });
  $stateProvider.state('main.portada', {
    url: '',
    template: '<portada></portada>'
  });
  $stateProvider.state('main.pregunta', {
    url: 'preguntas',
    template: '<pregunta></pregunta>'
  });
  $stateProvider.state('main.resultado', {
    url: 'resultados',
    template: '<resultado></resultado>',
    params: {
      general: true,
      result: null
    }
  });
  $stateProvider.state('main.politicaDePrivacidad', {
    url: 'politicaDePrivacidad',
    template: require('../components/politicaDePrivacidad/politicaDePrivacidad.html')
  });
}
