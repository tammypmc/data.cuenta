FROM node:10-jessie

ENV DEBIAN_FRONTEND noninteractive
ENV CODE /usr/src/app

RUN printf "deb http://archive.debian.org/debian/ jessie main\ndeb-src http://archive.debian.org/debian/ jessie main\ndeb http://security.debian.org jessie/updates main\ndeb-src http://security.debian.org jessie/updates main" > /etc/apt/sources.list
RUN apt-get update \
        && apt-get install -y \
                curl \
        && apt-get clean

WORKDIR $CODE

# Copy code to conatiner volume
COPY . .

RUN export PATH="$PATH:$HOME/node_modules/.bin"

COPY docker-entrypoint.sh /

ENTRYPOINT ["/docker-entrypoint.sh"]

# To run in production mode: 'gulp serve:dist'; in development mode: 'gulp serve'
CMD ["sh", "-c", "gulp serve"]
