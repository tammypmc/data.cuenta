'use strict';

import { parse, Transform, AsyncParser } from 'json2csv';
import path from 'path';
import { writeFileSync, createWriteStream, mkdirSync } from 'fs';
import Resultado from './api/resultado/resultado.model';
import Encuesta from './api/encuesta/encuesta.model';

/**
 * RESULTADOS.csv (1 sólo registro)
 * ------------------------------
 * CAT 1,    CAT 2,   CAT 3
 * 2.658,   2.1478,   1.569
 * 
 * 
 * ENCUESTA.csv (varios registros)
 * ------------------------------
 * FECHA,   CAT 1,    CAT 2,   CAT 3,  PREGUNTA-1, PREGUNTA-2, PREGUNTA-X
 * 15-07-2019 19:30,    2.658,   2.1478,    1.569,  SI, NO, NOSE, ...
 * 
 * Archivar los resultados por fecha de exportación
 * > Al ejecutar el script, generar una carpeta con la fecha del momento de ejecución
 * > y guardar dentro los archivos csv
 */
export function exportToCSV (cb) {
  let date = Date.now();
  let prefixPath = `./exported-data`;
  let folderPath = `${prefixPath}/${date}`;
  let opt = { date, folderPath };

  mkdir(prefixPath);
  mkdir(folderPath);

  exportResultadosToCSV(opt, () => {
    console.log('ResultadosCollection exported to CSV');
    exportEncuestaToCSV(opt, () => {
      console.log('EncuestaCollection exported to CSV');
      console.log('Done');
      cb();
    });
  });
}


export function exportResultadosToCSV(opt, cb) {
  Resultado.findOne((err, resultado) => {
    let csv = parse(resultado.toObject(), {
      fields: ['cat1', 'cat2', 'cat3'], delimiter: ';'
    });
    writeFileSync(path.resolve(opt.folderPath, 'resultados.csv'), csv);
    cb();
  });
}


export function exportEncuestaToCSV(opt, cb) {
  const docStream = Encuesta.find().cursor({
    transform: (doc) => {
      doc = doc.toObject();
      let newPreguntas = {};
      doc.preguntas.forEach(preg => {
        newPreguntas[preg.idPregunta] = preg.respuesta;
      });
      doc.preguntas = newPreguntas;
      Reflect.deleteProperty(doc, '__v');
      return doc;
    }
  });
  const writeStream = createWriteStream(path.resolve(opt.folderPath, 'encuesta.csv'));
  const json2csv = new Transform({
    flatten: true,
    delimiter: ';'
  }, {
    objectMode: true
  });

  docStream
    .pipe(json2csv)
    .pipe(writeStream)
    .on('finish', cb);
}


function mkdir(folderPath) {
  try {
    mkdirSync(path.resolve(folderPath));
  }
  catch(err) {
    if (err.code === 'EEXIST') {
      return;
    }
    console.error(err);
  }
}